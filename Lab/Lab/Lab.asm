.include "m8def.inc"

;���������
.equ BAUD = 9600 ;��������� �������� ��� UARD � �����
.equ fCK = 4000000 ;������� �� � ������ 
.equ UBRR_value = (fCK/(BAUD*8))-1 ; ���������� �������� ��� �������� UBRR

;���� ������
.dseg
.org $100
 name: .BYTE 6 

;������ ����� ����
.cseg
.org 0

;������� � ������� �������
rjmp main

;������� �� ���� ������
hi_str: .db "Privet, ",0
greating: .db "Hi, what is your name? ",0x0D,0

;������������

;����� ���������� �����
say_hi:
  ldi r23, 6
say_next:
  ld r16, Z+
  rcall USART_send
  dec r23
  brne say_next
  ret

;; ����������� USART � �����
init_USART:
  ldi R16, high(UBRR_value) ;���������� �������� 9600 ���
  out UBRRH, R16
  ldi R16, low(UBRR_value)
  out UBRRL, R16

  ldi R16, 2 ;����� �������� ������� ��� 1���
  out UCSRA, R16

  ldi R16,(1<<TXEN)|(1<<RXEN)
  out UCSRB, R16
  ldi R16, (1<<URSEL)|(1<<UCSZ0)|(1<<UCSZ1)
  out UCSRC,R16
  ret

;;������� ���� ����
USART_send:
  sbis UCSRA, UDRE
  rjmp USART_send
  out UDR, R16
  ret 
  
;;������� ���� ����
USART_read:
  sbis UCSRA, RXC
  rjmp USART_read
  in R16, UDR
  ret
  
;;������� � USART ������ �� Flash
send_flash_str:
  add r30,r30
  adc r31, r31
  
get_flash_byte:
  lpm r16, Z+
  cpi r16, 0
  breq send_str_end
  rcall USART_send
  rjmp get_flash_byte
  
send_str_end:
  ret

;��������� �������� ������
read_name:
  ldi r30, low(name)
  ldi r31, high(name)
  ldi r23, 6
read_next:
  rcall USART_read
  st Z+, r16
  ;rcall USART_send
  dec r23
  brne read_next
  ldi r16, 0x0D
  rcall USART_send
  ret

; �������� ���������
main:
  ldi R16, low(RAMEND)
  out SPL,R16
  ldi R17,high(RAMEND)
  out SPH, R17

  rcall init_USART
  
  ldi r30, low( greating )
  ldi r31, high( greating )
  rcall send_flash_str

  ; ��������� ������
  rcall read_name
  
  ldi r30, low( hi_str )
  ldi r31, high( hi_str )
  rcall send_flash_str

  ldi r30, low( name )
  ldi r31, high( name )
  rcall say_hi
  rjmp disp_end
disp_end:
  rjmp disp_end
