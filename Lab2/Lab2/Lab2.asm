
/*
	��������� ��������� �������� 5 ����������� ����������� �����, ����������� � ���. 
	���� � ���������� �������� ���������� �������-����� ��������� �����, ������� � ���� � ����� $FF. 
	���� ���������-��� ���, �� �������� ���������� � ���� D ���������� ����� $55 � $AA.
*/

.include "m16def.inc"
.def outPort = r15
.def op1 = r16
.def res = r17
.def trueRes1 = r22
.def trueRes2 = r23
.def falseRes = r24

rjmp init

init:

ldi res, 0

ldi trueRes1, $55
ldi trueRes2, $AA
ldi falseRes, $FF

ldi op1, $00FF
add res, op1
brvs overflow
ldi op1, $F0
add res, op1
brvs overflow
ldi op1, $F0
add res, op1
brvs overflow
ldi op1, $F0
add res, op1
brvs overflow
ldi op1, $F0
add res, op1
brvs overflow

circle:
out PORTB, trueRes1
out PORTB, trueRes2
rjmp circle

overflow:
out PORTB, falseRes